<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<!--This is an example home page for a specific class with 3 midterms and a final exam-->   

<html>
<header style="font-size: 24pt">
<title>Physics Problem Roulette</title>
Physics 140 Problem Roulette
<script src="trackingcode.js"></script>
</header> <br/>
<body style="font-size: 16pt">
The links below serve randomly-chosen questions, one at a time, from banks of multiple-choice problems derived from past exams.<br/> <br/>
Please select an exam to begin.  For each problem, you have the option to  submit your answer or skip to the next one.<br/><br/>
<a href="roulette.php?exam=140m1"> Physics 140 Midterm 1</a> <br/>
<a href="roulette.php?exam=140m2"> Physics 140 Midterm 2</a> <br/>
<a href="roulette.php?exam=140m3"> Physics 140 Midterm 3</a> <br/>
<a href="roulette.php?exam=140f"> Physics 140 Final Exam</a> <br/>
<a href="roulette.php?exam=140all"> All Physics 140 Exams</a> <br/> <br/>
<a href="index.html"> Return to Home Page</a>
</body>
</html>