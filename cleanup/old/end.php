</div>
<div class="tab-pane" id="statistics">
statistics!
</div>
</div>
     </div>
     <div id="push"></div>
   </div>
   <div id="footer">
      <div class="container">
        <p class="muted credit">
          Development of this site was sponsored by the <a href="http://www.provost.umich.edu" target="_blank">UM Office of the Provost</a> through the Gilbert Whitaker Fund for the Improvement of Teaching.
		</p>
        <p class="muted credit">
          Please send any feedback to <a href="mailto:physics.sso@umich.edu">physics.sso@umich.edu</a><br/>
          For issues with the content of the problems, see your instructor first.
        </p>
      </div>
    </div>
    
    <script src="js/bootstrap.js"></script>
  </body>
</html>