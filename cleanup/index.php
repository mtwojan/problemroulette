<?php include 'start.php'; ?>
<p>
  Welcome! This site serves random problems from past exams given in introductory courses of the University of Michigan Department of Physics.
</p>

<img class="logo" src="img/PR.jpg"></img>

<p><strong>Please select your class to begin:</strong></p>
<div class="button-container">
  <a class="btn" href="Home140.php">Physics 140</a><br/>
  <a class="btn" href="Home240.php">Physics 240</a><br/>
  <a class="btn" href="Home135.php">Physics 135</a><br/>
  <a class="btn" href="Home235.php">Physics 235</a><br/>
</div>

<?php include 'end.php'; ?>
        